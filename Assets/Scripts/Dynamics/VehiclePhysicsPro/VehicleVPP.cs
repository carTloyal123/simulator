﻿/**
 * Copyright (c) 2021 LG Electronics, Inc.
 *
 * This software contains code licensed as described in LICENSE.
 *
 */

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VehiclePhysics;

public class VehicleVPP : MonoBehaviour, IVehicleDynamics
{
    public Text uiText;

    public Rigidbody RB { get; private set; }
    private Vector3 lastRBPosition;

    private VehicleVPPControllerInput vehicleVPPControllerInput;

    // combined throttle and brake input
    public Vector3 Velocity => RB.velocity;
    public Vector3 AngularVelocity => RB.angularVelocity;

    public Transform BaseLink
    {
        get { return BaseLinkTransform; }
    }

    public Transform BaseLinkTransform;

    public float AccellInput { get; set; } = 0f;
    public float SteerInput { get; set; } = 0f;

    public bool HandBrake { get; set; } = false;
    public float CurrentRPM { get; set; } = 0f;

    public float CurrentGear { get; set; } = 1f;
    public bool Reverse { get; set; } = false;

    public List<VPAxleInfo> Axles = new List<VPAxleInfo>();

    public float WheelAngle
    {
        get
        {
            if (Axles != null && Axles.Count > 0 && Axles[0] != null)
            {
                return (Axles[0].Left.steerAngle + Axles[0].Right.steerAngle) * 0.5f;
            }

            return 0.0f;
        }
    }

    public float Speed
    {
        get
        {
            if (RB != null)
            {
                return RB.velocity.magnitude * METERS_PER_SECOND_TO_MILES_PER_HOUR;
            }

            return 0f;
        }
    }

    public float MeasuredSpeed { get; set; }

    public float MaxSteeringAngle { get; set; } = 39.4f;

    public IgnitionStatus CurrentIgnitionStatus { get; set; }

    private IAgentController vehicleController;


    private const float METERS_PER_SECOND_TO_MILES_PER_HOUR = 2.23693629f;


    private void Awake()
    {
        RB = GetComponent<Rigidbody>();
        vehicleController = GetComponent<IAgentController>();
        vehicleVPPControllerInput = GetComponent<VehicleVPPControllerInput>();
        vehicleVPPControllerInput.SetGear(1);
        UpdateGearFromVpp();
        vehicleVPPControllerInput.SetIgnition(1);
    }

    // NOTE: Wheel colliders need work

    public void Init()
    {
        // TODO: call this from vehicleVPPControllerInput or vehiclecontroller to set up VPP stuff ???

        // if (!isInitialized)
        //      return

        // isInitialized = true;

    }
    void OnEnable()
    {
        lastRBPosition = RB.position;
    }

    public void FixedUpdate()
    {
        if (vehicleController != null)
        {
            SteerInput = vehicleController.SteerInput;
            AccellInput = vehicleController.AccelInput;
        }

        vehicleVPPControllerInput.SetSteer(SteerInput);

        vehicleVPPControllerInput.SetHandBrake(HandBrake);

        if (AccellInput > 0)
        {
            vehicleVPPControllerInput.SetAccel(AccellInput);
            vehicleVPPControllerInput.SetBrake(0);
        }
        else
        {
            vehicleVPPControllerInput.SetAccel(0);
            vehicleVPPControllerInput.SetBrake(Math.Abs(AccellInput));
        }

        MeasuredSpeed = ((RB.position - lastRBPosition) / Time.fixedDeltaTime).magnitude;
        lastRBPosition = RB.position;

        UpdateGearFromVpp();

        CurrentRPM = vehicleVPPControllerInput.GetRPMEngine();
    }

    bool IVehicleDynamics.GearboxShiftUp()
    {
        vehicleVPPControllerInput.GearShiftUpAuto();

        UpdateGearFromVpp();
        return false;
    }

    bool IVehicleDynamics.GearboxShiftDown()
    {
        vehicleVPPControllerInput.GearShiftDownAuto();

        UpdateGearFromVpp();
        return false;
    }

    public bool ShiftFirstGear()
    {
        vehicleVPPControllerInput.SwitchToFirstGearFromReverse();

        UpdateGearFromVpp();
        return false;
    }

    public bool ShiftReverse()
    {
        vehicleVPPControllerInput.SwitchToReverse();

        UpdateGearFromVpp();
        return false;
    }

    public bool ToggleReverse()
    {
        if (Reverse)
        {
            ShiftFirstGear();
        }
        else
        {
            ShiftReverse();
        }

        return false;
    }

    public bool ShiftReverseAutoGearBox()
    {
        vehicleVPPControllerInput.GearShiftDownAuto();

        UpdateGearFromVpp();
        return false;
    }

    public bool ToggleIgnition()
    {
        switch (CurrentIgnitionStatus)
        {
            case IgnitionStatus.Off:
                StartEngine();
                break;
            case IgnitionStatus.On:
                StopEngine();
                break;
        }

        return false;
    }

    public bool ToggleHandBrake()
    {
        HandBrake = !HandBrake;
        return false;
    }

    public bool SetHandBrake(bool handBrakeIsActive)
    {
        HandBrake = handBrakeIsActive;
        return false;
    }

    public bool ForceReset(Vector3 pos, Quaternion rot)
    {
        RB.MovePosition(pos);
        RB.MoveRotation(rot);
        RB.velocity = Vector3.zero;
        RB.angularVelocity = Vector3.zero;

        var vpVehicleController = GetComponent<VPVehicleController>();
        vpVehicleController.enabled = false;
        vpVehicleController.enabled = true;

        vehicleVPPControllerInput.SetGear(1);
        CurrentGear = 1;

        CurrentRPM = vehicleVPPControllerInput.GetRPMEngine();
        AccellInput = 0f;
        SteerInput = 0f;

        HandBrake = false;

        vehicleVPPControllerInput.SetSteer(SteerInput);
        vehicleVPPControllerInput.SetHandBrake(HandBrake);
        vehicleVPPControllerInput.SetAccel(AccellInput);
        return false;
    }

    public bool GearboxShiftUp()
    {
        vehicleVPPControllerInput.GearShiftUpAuto();

        UpdateGearFromVpp();
        return false;
    }

    private void UpdateGearFromVpp()
    {
        int gearNew = vehicleVPPControllerInput.GetGear();

        Reverse = gearNew == -1;

        CurrentGear = Math.Abs(gearNew);
    }

    public void StartEngine()
    {
        vehicleVPPControllerInput.SetIgnition(1);
        CurrentIgnitionStatus = IgnitionStatus.On;
    }

    public void StopEngine()
    {
        vehicleVPPControllerInput.SetIgnition(-1);
        CurrentIgnitionStatus = IgnitionStatus.Off;
    }
}