/**
 * Copyright (c) 2021 LG Electronics, Inc.
 *
 * This software contains code licensed as described in LICENSE.
 *
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VehiclePhysics;

public class VehicleVPPControllerInput : VehicleBehaviour
{
    public VehicleBase m_vehicle;

    public override void OnEnableVehicle()
    {
        // This component requires a MyVpp explicitly

        //m_vehicle = vehicle.GetComponent<VPVehicleController>();
        if (m_vehicle == null)
        {
            DebugLogWarning("A vehicle based on VehicleController is required. Component disabled.");
            enabled = false;
        }
    }

    public override void UpdateVehicle()
    {
        // TODO: Fix this stuff to get input from simulator and send to VPP ???

        // Read the input from the standard Unity Input

        //float steerInput = Mathf.Clamp(Input.GetAxis("Horizontal"), -1.0f, 1.0f);

        //float throttleAndBrakeAxisValue = Input.GetAxis("Vertical");
        //float throttleInput = Mathf.Clamp01(throttleAndBrakeAxisValue);
        //float brakeInput = Mathf.Clamp01(-throttleAndBrakeAxisValue);

        //// Hold Ctrl and Brake for reverse

        //if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        //{
        //    throttleInput = -brakeInput;
        //    brakeInput = 0.0f;
        //}

        //// Feed the vehicle input parameters with the result

        //m_vehicle.steerInput = steerInput;
        //m_vehicle.driveInput = throttleInput;
        //m_vehicle.brakeInput = brakeInput;
    }


    public void SetSteer(float steerNormalized)
    {
        int steerVpp = (int)(steerNormalized * 10000);
        m_vehicle.data.Set(Channel.Input, InputData.Steer, steerVpp);
    }

    public void SetHandBrake(bool handBrakeIsActive)
    {
        int handBrakeLevel = 0;
        if (handBrakeIsActive)
        {
            handBrakeLevel = 10000;
        }

        m_vehicle.data.Set(Channel.Input, InputData.Handbrake, handBrakeLevel);
    }

    public void SetAccel(float accelNormalized)
    {
        int throttleVpp = (int)(accelNormalized * 10000);
        m_vehicle.data.Set(Channel.Input, InputData.Throttle, throttleVpp);
    }

    public void SetBrake(float brakeNormalized)
    {
        int brakeVpp = (int)(brakeNormalized * 10000);
        m_vehicle.data.Set(Channel.Input, InputData.Brake, brakeVpp);
    }

    public void GearShiftUpAuto()
    {
        int gearCurrent = m_vehicle.data.Get(Channel.Vehicle, VehicleData.GearboxGear);

        if (gearCurrent == -1)
        {
            // Set the gear to 1
            m_vehicle.data.Set(Channel.Input, InputData.ManualGear, 1);
            return;
        }

        // Increase the gear by 1
        m_vehicle.data.Set(Channel.Input, InputData.GearShift, 1);
    }

    public void GearShiftDownAuto()
    {
        int gearCurrent = m_vehicle.data.Get(Channel.Vehicle, VehicleData.GearboxGear);

        if (gearCurrent == 1)
        {
            // Set the gear to -1
            m_vehicle.data.Set(Channel.Input, InputData.ManualGear, -1);
            return;
        }

        // Decrease the gear by 1
        m_vehicle.data.Set(Channel.Input, InputData.GearShift, -1);
    }


    public void SetIgnition(int state)
    {
        m_vehicle.data.Set(Channel.Input, InputData.Key, state);
    }

    public void SwitchToFirstGearFromReverse()
    {
        int gearCurrent = m_vehicle.data.Get(Channel.Vehicle, VehicleData.GearboxGear);
        if (gearCurrent != -1)
        {
            return;
        }

        // Set the gear to 1
        m_vehicle.data.Set(Channel.Input, InputData.ManualGear, 1);
    }


    public void SwitchToReverse()
    {
        // Set the gear to -1
        m_vehicle.data.Set(Channel.Input, InputData.ManualGear, -1);
    }


    public int GetGear()
    {
        return m_vehicle.data.Get(Channel.Vehicle, VehicleData.GearboxGear);
    }

    public void SetGear(int gear)
    {
        m_vehicle.data.Set(Channel.Input, InputData.GearShift, gear);
    }

    public int GetRPMEngine()
    {
        return (int)(m_vehicle.data.Get(Channel.Vehicle, VehicleData.EngineRpm) / 1000.0f);
    }

}
